const fs = require("fs");
const process = require("process");
const levenshtein = require("./levenshtein")

if (process.argv.length <= 2) { console.log("Usage: node ask <quesionset>\nExample: node ask db2"); process.exit(1); }
const questions = require("./" + process.argv[2] + ".json");
const state = fs.existsSync("./" + process.argv[2] + ".state.json") ? require("./" + process.argv[2] + ".state.json") : {};

const next = [];
let readBool = false;
let readString = false;
let stringBuffer = "";
require("readline").emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on("keypress", function(chunk, key) {
  if (key && ((key.name === "q" && readBool) || (key.name === "c" && key.ctrl))) { console.log(); printStats(); process.exit(); }
  else if (readBool && key && key.name === "n") { next.push(false); process.stdout.write("NO"); readBool = false; }
  else if (readBool && key && (key.name === "y" || key.name === "j")) { next.push(true); process.stdout.write("YES"); readBool = false; }
  else if (readString && key.name == "backspace" && stringBuffer.length) { process.stdout.write("\010 \010"); }
  else if (readString && key.name != "return") process.stdout.write(chunk);
});
process.stdin.on("data", function(chunk) {
  if (!readString) return;
  if (chunk[0] == 0x0d) { next.push(stringBuffer); readString = false; return; }
  if (chunk[0] == 0x7f) { stringBuffer = stringBuffer.substr(0, stringBuffer.length - 1); return; }
  chunk = chunk.filter(x => x >= 32);
  stringBuffer += chunk.toString();
})

let answered = 0, correct = 0;
function printStats() {
  if (answered === 0) return;
  console.log();
  console.log("\033[1;36m  -> " + correct + "/" + answered + " (" + Math.round(correct * 10000 / answered) / 100 + "%)");
  console.log();
}

async function main() {
  for (category in questions) {
    console.log("\033[1;44m " + category + " \033[0m");
    const ql = Object.keys(questions[category]);
    shuffleArray(ql);
    for (question of ql) {
      if (getState(category, question) > 3) continue;
      process.stdout.write("\033[1m" + question + "\033[0m (" + getState(category, question) + "/3) \033[1;35m");
      let correctAnswer = questions[category][question];
      if (typeof correctAnswer == "boolean" || typeof correctAnswer == "string") correctAnswer = [correctAnswer];
      wrong = false;
      if (correctAnswer.length > 1) process.stdout.write("\n");
      for (let i in correctAnswer) {
        if (correctAnswer.length > 1) process.stdout.write(" \033[1;35m" + (i*1+1) + ". ");
        const answer = typeof correctAnswer[i] == "boolean" ? await getAnswer() : await getString();
        answered++;
        const isCorrect = typeof correctAnswer[i] == "boolean" ? answer === correctAnswer[i] : answer.replace(/\s+/g, "").toLowerCase() == correctAnswer[i].replace(/\s+/g, "").toLowerCase();
        if (isCorrect) { correct++; console.log(" \033[1;32m✓\033[0m"); }
        else if (typeof correctAnswer[i] == "string" && levenshtein.get(answer.replace(/\s+/g, "").toLowerCase(), correctAnswer[i].replace(/\s+/g, "").toLowerCase()) <= Math.floor(correctAnswer[i].length / 5)) { correct++; console.log(" \033[1;33m✓\033[0m"); }
        else { wrong = true; console.log(" \033[1;31m✗" + (typeof correctAnswer[i] == "boolean" ? "" : " " + correctAnswer[i]) + "\033[0m"); }
      }
      updateState(category, question, !wrong);
    }
  }
  printStats();
  process.exit();
}

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

function getState(category, question) {
  if (state[category]) {
    if (state[category][question]) {
       return state[category][question];
    }
  } else {
    state[category] = {};
  }
  state[category][question] = 1;
  return 1;
}

function updateState(category, question, success) {
  const old = getState(category, question);
  if (success) state[category][question]++;
  else state[category][question] = 1;
  fs.writeFileSync("db2.state.json", JSON.stringify(state));
}

async function getAnswer() {
  readBool = true;
  while (next.length == 0) await new Promise(r => setTimeout(() => r(), 25));
  return next.shift();
}
async function getString() {
  readString = true; stringBuffer = "";
  while (next.length == 0) await new Promise(r => setTimeout(() => r(), 25));
  readString = false;
  return next.shift();
}

main();
